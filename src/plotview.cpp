/*
 * Medical diaries for Sailfish OS.
 * Copyright 2016-2017 EwerestMD LLC.
 * All rights reserved.
 */

#include "plotview.h"

#include <QPainter>
#include <QtMath>

static const QFont PLOT_TEXT_FONT(QFont().family(), 18);
static const float MIN_STEP = 1;

// PlotView

/*!
 * \brief Constructor. Initializes the plotPeriodList with available periods.
 * \param parent parent QQuickItem of the view
 */
PlotView::PlotView(QQuickItem* parent) : QQuickPaintedItem(parent) {
    QDate today = QDate::currentDate();
    plotPeriodList.append(PlotPeriod{static_cast<int>(today.addDays(-7).daysTo(today)) - 1, 1});
    plotPeriodList.append(PlotPeriod{static_cast<int>(today.addMonths(-1).daysTo(today)) - 1, 1});
    plotPeriodList.append(PlotPeriod{static_cast<int>(today.addMonths(-3).daysTo(today)) - 1, 3});
    plotPeriodList.append(PlotPeriod{static_cast<int>(today.addYears(-1).daysTo(today)) - 1, 12});
    daoInstance = new DataAccessObject();
}

/*!
 * \brief The destructor.
 */
PlotView::~PlotView() {
    delete daoInstance;
    daoInstance = NULL;
}

/*!
 * \brief Clears the plot, fills plot lines and triggers the view paint event.
 */
void PlotView::drawPlot() {
    plotLineList.clear();
    QVariantMap leftBoundRecord =
        daoInstance->retrieveLastBloodPressureBeforeDate(calculatePeriodStartDate());
    QVariantMap rightBoundRecord =
        daoInstance->retrieveFirstBloodPressureAfterDate(calculatePeriodEndDate());
    addPlotLine(leftBoundRecord, rightBoundRecord, "systolic", QColor(Qt::blue));
    addPlotLine(leftBoundRecord, rightBoundRecord, "diastolic", QColor(Qt::green));
    addPlotLine(leftBoundRecord, rightBoundRecord, "pulse", QColor(Qt::red));
    emit plotVisibleChanged();
    update();
}

/*!
 * \brief Add the average values of the specified field to the plot line points list. Records are
 * retrieved in descending order sorted by datetime. Left and right bound records are added
 * to the created list to beginning and to end accordingly.
 * \param leftBoundRecord the first record found before the current period
 * \param rightBoundRecord the first record found after the current period
 * \param fieldName name of the field to retrieve its average values
 * \param color color used to draw the plot line values
 */
void PlotView::addPlotLine(const QVariantMap& leftBoundRecord, const QVariantMap& rightBoundRecord,
                           const QString& fieldName, const QColor& color) {
    PlotLine plotLine;
    fillPlotPointsList(plotLine, leftBoundRecord, rightBoundRecord, fieldName);
    if (plotLine.plotPoints.isEmpty()) {
        return;
    } else {
        plotLine.min = plotLine.max = plotLine.plotPoints[0]["value"].toFloat();
    }
    foreach (QVariantMap plotPoint, plotLine.plotPoints) {
        float value = plotPoint["value"].toFloat();
        if (plotLine.min > value) plotLine.min = value;
        if (plotLine.max < value) plotLine.max = value;
    }
    plotLine.color = color;
    plotLineList.append(plotLine);
}

/*!
 * \brief Fills the given plot line with records and checks whether
 * left and right bound records exist.
 * \param plotLine plot line to fill
 * \param leftBoundRecord the first record found before the current period
 * \param rightBoundRecord the first record found after the current period
 * \param fieldName name of the field to retrieve its average values
 */
void PlotView::fillPlotPointsList(PlotLine& plotLine, const QVariantMap& leftBoundRecord,
                                  const QVariantMap& rightBoundRecord, const QString& fieldName) {
    plotLine.rightBoundRecordExists = !rightBoundRecord.isEmpty();
    if (plotLine.rightBoundRecordExists) {
        plotLine.plotPoints.append(QVariantMap{{"value", rightBoundRecord[fieldName]}, {"datetime",
                rightBoundRecord["datetime"]
            }});
    }
    plotLine.plotPoints.append(retrievePlotPointsList(fieldName));
    plotLine.leftBoundRecordExists = !leftBoundRecord.isEmpty();
    if (plotLine.leftBoundRecordExists) {
        plotLine.plotPoints.append(QVariantMap{{"value", leftBoundRecord[fieldName]}, {"datetime",
                leftBoundRecord["datetime"]
            }});
    }
}

/*!
 * \brief Retrieves the displayed plot points from the database.
 * \param fieldName name of the field to retrieve its values
 * \return list of retrieved plot points that contain value and datetime
 */
QList<QVariantMap> PlotView::retrievePlotPointsList(const QString& fieldName) const {
    QList<QVariantMap> plotPoints;
    if (periodIndex() == 0) {
        plotPoints = daoInstance->retrievePlotPointsForWeek(fieldName, calculatePeriodEndDate());
    } else {
        plotPoints = daoInstance->retrievePlotPoints(fieldName, calculatePeriodStartDate(),
                     calculatePeriodEndDate(),
                     plotPeriodList[periodIndex()].groupDays);
    }
    return plotPoints;
}

// Calculations

/*!
 * \brief Calculates adjusted min, max and step values.
 * Min and max values are calculated using min and max values from the plot lines and adjusted to
 * enlarge the min-max range by 20%.
 */
void PlotView::calculatePlotAttributes() {
    float min = plotLineList[0].min;
    float max = plotLineList[0].max;
    foreach (PlotLine plotLine, plotLineList) {
        if (min > plotLine.min) min = plotLine.min;
        if (max < plotLine.max) max = plotLine.max;
    }
    step = qMax(qCeil((max - min) * 1.2 / verticalStepsCount() / MIN_STEP) * MIN_STEP, MIN_STEP);
    minValue = calculateMinValue(step, min, max);
    maxValue = minValue + step * verticalStepsCount();
}

/*!
 * \brief Calculates the vertical step count depending on the screen height.
 * \return amount of vertical steps to display
 */
int PlotView::verticalStepsCount() const {
    return qFloor(height() - calculateTimeLabelHeight() * 2) / (calculateTimeLabelHeight() * 3);
}

/*!
 * \brief Calculates the horizontal step count depending on the screen width.
 * \return amount of horizontal steps to display
 */
int PlotView::horizontalStepsCount() const {
    return periodIndex() == 0 ? 8 : qFloor(width() / (calculateTimeLabelWidth() * 1.5));
}

/*!
 * \brief Calculates the adjusted min value by substracting the half amount of steps from the
 * average of min-max range.
 * \param step step between vertical labels
 * \param min min value of the plot
 * \param max max value of the plot
 * \return adjusted min value
 */
float PlotView::calculateMinValue(float step, float min, float max) const {
    return qRound((min + max - step * verticalStepsCount()) / 2 / MIN_STEP) * MIN_STEP;
}

/*!
 * \brief Calculates the point coordinates from plotPoint datetime and value
 * to display on the view.
 * \param plotPoint QVariantMap with datetime and value of the point
 * \return QPoint with coordinates to display on the view
 */
QPoint PlotView::calculatePlotPoint(const QVariantMap& plotPoint) const {
    return QPoint(calculatePlotXCoordinate(plotPoint["datetime"].toDateTime()),
                  calculatePlotYCoordinate(plotPoint["value"].toFloat()));
}

/*!
 * \brief Calculates the x coordinate of the plot point using datetime.
 * \param datetime datetime to convert to x coordinate
 * \return x coordinate of the plot point
 */
int PlotView::calculatePlotXCoordinate(const QDateTime& datetime) const {
    QDateTime startDate = QDateTime(calculatePeriodStartDate(), QTime(0, 0, 0, 0));
    QTime endTime = periodIndex() == 0 ? QTime(23, 59, 59, 999) : QTime(0, 0, 0, 0);
    QDateTime endDate = QDateTime(calculatePeriodEndDate(), endTime);
    float ratio = 1.0 * startDate.secsTo(datetime) / startDate.secsTo(endDate);
    int xValue = qRound((width() - calculateTimeLabelWidth() * 2) * ratio);
    if (ratio < 0) return xValue - calculateTimeLabelWidth();
    if (ratio > 1) return xValue + calculateTimeLabelWidth() * 3;
    return xValue + calculateTimeLabelWidth();
}

/*!
 * \brief Calculates the y coordinate of the plot point using given value.
 * \param value value to convert to y coordinate
 * \return y coordinate of the plot point
 */
int PlotView::calculatePlotYCoordinate(float value) const {
    int plotHeight = height() - calculateTimeLabelHeight() * 2;
    return plotHeight - (value - minValue) / (maxValue - minValue) * plotHeight;
}

/*!
 * \brief Calculates and returns the width of the dd.MM string using default font.
 * \return width of the day.month string
 */
int PlotView::calculateTimeLabelWidth() const {
    return QFontMetrics(PLOT_TEXT_FONT).width("00.00");
}

/*!
 * \brief Calculates and returns the height of the dd.MM string using default font.
 * \return width of the day.month string
 */
int PlotView::calculateTimeLabelHeight() const {
    return QFontMetrics(PLOT_TEXT_FONT).height();
}

/*!
 * \brief Calculates the period start date by subtracting the period length
 * from the period end date.
 * \return start date of the period
 */
QDate PlotView::calculatePeriodStartDate() const {
    return calculatePeriodEndDate().addDays(-plotPeriodList[periodIndex()].daysLength);
}

/*!
 * \brief Calculates the period end date by adding the period length to the current date.
 * Amount of added periods is defined by the offset field value.
 * \return end date of the period
 */
QDate PlotView::calculatePeriodEndDate() const {
    return QDate::currentDate()
           .addDays(-(plotPeriodList[periodIndex()].daysLength + 1) * offsetValue);
}

// Accessors

/*!
 * \brief Sets index of the current period.
 * \param periodIndex index of the period to set
 */
void PlotView::setPeriodIndex(int periodIndex) {
    periodIndexValue = periodIndex;
    offsetValue = 0;
    emit periodIndexChanged();
    emit periodYearsChanged();
}

/*!
 * \return current period index
 */
int PlotView::periodIndex() const {
    return periodIndexValue;
}

/*!
 * \brief Sets offset of the current period.
 * \param offset offset of the period to set
 */
void PlotView::setOffset(int offset) {
    offsetValue = offset;
    emit offsetChanged();
    emit periodYearsChanged();
}

/*!
 * \return current offset
 */
int PlotView::offset() const {
    return offsetValue;
}

/*!
 * \return plot min value
 */
float PlotView::getMinValue() const {
    return minValue;
}

/*!
 * \return plot max value
 */
float PlotView::getMaxValue() const {
    return maxValue;
}

/*!
 * \return step between the vertical labels
 */
float PlotView::getStep() const {
    return step;
}

/*!
 * \brief Increment the offset if left bound record exist.
 */
void PlotView::incrementOffset() {
    if (plotLineList.isEmpty()) return;
    if (!plotLineList[0].leftBoundRecordExists) return;
    setOffset(offsetValue + 1);
}

/*!
 * \brief Decrement the offset if the period end date is less than the current date.
 */
void PlotView::decrementOffset() {
    if (calculatePeriodEndDate() >= QDate::currentDate()) return;
    setOffset(offsetValue - 1);
}

/*!
 * \brief Returns true if plotLineList is not empty i.e. PlotView has data to display.
 * \return true if plot is visible
 */
bool PlotView::plotVisible() const {
    return !plotLineList.isEmpty();
}

/*!
 * \brief Creates the year string which represents the year interval between period start date
 * and end date. If period fits a single calendar year this year is returned as string.
 * \return years of the displayed period
 */
QString PlotView::periodYears() const {
    int startYear = calculatePeriodStartDate().year();
    int endYear = calculatePeriodEndDate().year();
    if (startYear == endYear) {
        return QString::number(endYear);
    } else {
        return QString("%1 – %2").arg(startYear).arg(endYear);
    }
}

// Drawing

/*!
 * \brief Draws the plot on the screen.
 * \param painter painter used to draw the plot
 */
void PlotView::paint(QPainter* painter) {
    if (plotLineList.isEmpty()) return;
    calculatePlotAttributes();
    painter->setRenderHints(QPainter::Antialiasing);
    drawVerticalScaleLines(painter);
    drawHorizontalScaleMarkers(painter);
    drawHorizontalScaleLabels(painter);
    drawLines(painter);
    drawVerticalScaleLabels(painter);
}

/*!
 * \brief Draws lines with measurements on the plot.
 * \param painter painter used to draw the plot
 */
void PlotView::drawLines(QPainter* painter) {
    foreach (PlotLine plotLine, plotLineList) {
        painter->setPen(QPen(QBrush(plotLine.color), 5));
        QPainterPath path;
        QPoint firstPoint = calculatePlotPoint(plotLine.plotPoints.first());
        path.moveTo(firstPoint);
        painter->setBrush(QBrush(plotLine.color));
        painter->drawEllipse(firstPoint, 5, 5);
        painter->drawEllipse(calculatePlotPoint(plotLine.plotPoints.last()), 5, 5);
        foreach (QVariantMap plotPoint, plotLine.plotPoints) {
            QPoint point = calculatePlotPoint(plotPoint);
            path.lineTo(point);
            if (periodIndex() == 0) painter->drawEllipse(point, 5, 5);
        }
        painter->setBrush(QBrush(Qt::transparent));
        painter->drawPath(path);
    }
}

/*!
 * \brief Draws vertical scale lines.
 * \param painter painter used to draw the plot
 */
void PlotView::drawVerticalScaleLines(QPainter* painter) {
    painter->setPen(QPen(QBrush(Qt::white), 1));
    for (float i = minValue; i < maxValue; i += step) {
        int y = calculatePlotYCoordinate(i);
        painter->drawLine(0, y, width(), y);
    }
}

/*!
 * \brief Draws vertical scale labels.
 * \param painter painter used to draw the plot
 */
void PlotView::drawVerticalScaleLabels(QPainter* painter) {
    painter->setPen(QPen(QBrush(Qt::white), 1));
    painter->setFont(PLOT_TEXT_FONT);
    for (float i = minValue; i < maxValue; i += step) {
        painter->drawText(0, calculatePlotYCoordinate(i) - calculateTimeLabelHeight() * 0.2,
                          QString::number(i));
    }
}

/*!
 * \brief Draws horizontal scale markers.
 * \param painter painter used to draw the plot
 */
void PlotView::drawHorizontalScaleMarkers(QPainter* painter) {
    painter->setPen(QPen(QBrush(Qt::white), 1));
    int horizontalStep = (width() - calculateTimeLabelWidth() * 2) / (horizontalStepsCount() - 1);
    for (int i = 0; i < horizontalStepsCount(); i++) {
        int x = horizontalStep * i + calculateTimeLabelWidth();
        int y = height() - calculateTimeLabelHeight() * 2;
        painter->drawLine(x, y, x, y + calculateTimeLabelHeight() / 2);
    }
}


/*!
 * \brief Draws horizontal scale labels.
 * \param painter painter used to draw the plot
 */
void PlotView::drawHorizontalScaleLabels(QPainter* painter) {
    painter->setPen(QPen(QBrush(Qt::white), 1));
    painter->setFont(PLOT_TEXT_FONT);
    int timeLabelsCount = horizontalStepsCount();
    int horizontalStep = (width() - calculateTimeLabelWidth() * 2) / (timeLabelsCount - 1);
    if (periodIndex() == 0) timeLabelsCount--;
    QDate endDate = calculatePeriodEndDate();
    float daysGap = 1.0 * calculatePeriodStartDate().daysTo(endDate) / (timeLabelsCount - 1);
    for (int i = 0; i < timeLabelsCount; i++) {
        int labelOffset = -calculateTimeLabelWidth() / 2;
        if (periodIndex() == 0) labelOffset += horizontalStep / 2;
        int x = horizontalStep * i + calculateTimeLabelWidth() + labelOffset;
        int y = height() - calculateTimeLabelHeight() / 2;
        painter->drawText(x, y, endDate.addDays(
                              -qRound(daysGap * (timeLabelsCount - i - 1))).toString("dd.MM"));
    }
}
