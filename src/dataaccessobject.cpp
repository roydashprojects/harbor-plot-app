#include "dataaccessobject.h"

DataAccessObject::DataAccessObject(QObject* parent) : QObject(parent) {
    fillData();
}

void DataAccessObject::fillData() {
    QFile file("/usr/share/harbour-plot-app/assets/blood_pressure_and_pulse_values.txt");
    file.open(QFile::ReadOnly);
    QTextStream out(&file);
    for (int i = 0; i < 1000; i++) {
        QString string;
        out >> string;
        QStringList measurements = string.split(";");
        QVariantMap bloodPressure;
        bloodPressure["systolic"] = measurements[0];
        bloodPressure["diastolic"] = measurements[1];
        bloodPressure["pulse"] = measurements[2];
        bloodPressure["datetime"] = QDateTime::currentDateTime().addDays(-i).toString(Qt::ISODate);
        bloodPressureList.append(bloodPressure);
    }
    file.close();
}

/*!
 * \brief Retrieves the list of plot points from the database for given field. Points represent
 * average values of given field within given interval and grouped by given amount of days.
 * Value is the group average and date is the group end date.
 * \param fieldName name of the field to retrieve its values
 * \param startDate start of the date interval
 * \param endDate end of the date interval
 * \param groupDays number of days for which average values should be calculated
 * \return list of plot points defined by value and datetime
 */
QList<QVariantMap> DataAccessObject::retrievePlotPoints(const QString& fieldName,
        const QDate& startDate, const QDate& endDate, int groupDays) const {
    QList<QVariantMap> groupedPlotPointList;
    QList<QVariantMap> plotPointList;
    for (int i = 0; i < 1000; i++) {
        if (bloodPressureList.at(i)["datetime"] >= QDateTime(startDate, QTime(0, 0, 0, 0)) &&
                bloodPressureList.at(i)["datetime"] <= QDateTime(endDate, QTime(23, 59, 59))) {
            QVariantMap plotPoint;
            plotPoint["value"] =  bloodPressureList.at(i)[fieldName];
            int dateGroup = bloodPressureList.at(i)["datetime"].toDate().daysTo(endDate) / groupDays;
            plotPoint["datetime"] = QDateTime(endDate.addDays(-dateGroup * groupDays), QTime(0, 0, 0, 0));
            plotPointList.append(plotPoint);
        }
    }
    QDateTime dateOfGroup = plotPointList.at(0)["datetime"].toDateTime();
    int sum = 0, count = 0;
    for (int i = 0; i < plotPointList.count(); i++) {
        sum += plotPointList.at(i)["value"].toInt();
        count++;
        if (i + 1 == plotPointList.count() || plotPointList.at(i + 1)["datetime"].toDateTime() != dateOfGroup) {
            QVariantMap plotPoint;
            plotPoint["value"] =  sum / count;
            plotPoint["datetime"] = dateOfGroup;
            groupedPlotPointList.append(plotPoint);
            sum = 0;
            count = 0;
            if (i + 1 < plotPointList.count()) {
                dateOfGroup = plotPointList.at(i + 1)["datetime"].toDateTime();
            }
        }
    }
    return groupedPlotPointList;
}

/*!
 * \brief Retrieves the list of plot points from the database for week period
 * defined by the end date.
 * \param fieldName name of the field to retrieve its values
 * \param endDate end of the week interval
 * \return list of plot points defined by value and datetime
 */
QList<QVariantMap> DataAccessObject::retrievePlotPointsForWeek(const QString& fieldName,
        const QDate& endDate) const {
    QList<QVariantMap> plotPointList;
    for (int i = 0; i < 1000; i++) {
        if (bloodPressureList.at(i)["datetime"] >= endDate.addDays(-6) &&
                bloodPressureList.at(i)["datetime"] < endDate.addDays(1)) {
            QVariantMap plotPoint;
            plotPoint["value"] =  bloodPressureList.at(i)[fieldName];
            plotPoint["datetime"] = bloodPressureList.at(i)["datetime"];
            plotPointList.append(plotPoint);
        }
    }
    return plotPointList;
}

/*!
 * \brief Retrieves the first occurrence of the blood pressure record beyond the date.
 * \param date date to search for records beyond it
 * \param lookAfter true if should look after the given date or false if should look before
 * \return QVariantMap with systolic, diastolic, pulse and datetime fields of the found record.
 * Returns empty QVariantMap if the record is not found.
 */
QVariantMap DataAccessObject::retrieveFirstBloodPressureBeyondDate(const QDate& date,
        bool lookAfter) const {
    QDateTime datetime(date, QTime(0, 0, 0, 0));
    if (lookAfter) datetime = datetime.addDays(1);
    for (int i = 0; i < 1000; i++) {
        if ((lookAfter && bloodPressureList.at(i)["datetime"].toDateTime() >= datetime) ||
                (!lookAfter && bloodPressureList.at(i)["datetime"].toDateTime() < datetime)) {
            return bloodPressureList.at(i);
        }
    }
    return QVariantMap();
}

/*!
 * \brief Retrieves the first found blood pressure record before the given date.
 * \param date date to search for records before it
 * \return QVariantMap with systolic, diastolic, pulse and datetime fields of the found record.
 * Returns empty QVariantMap if the record is not found.
 */
QVariantMap DataAccessObject::retrieveLastBloodPressureBeforeDate(const QDate& date) const {
    return retrieveFirstBloodPressureBeyondDate(date, false);
}

/*!
 * \brief Retrieves the first found blood pressure record after the given date.
 * \param date date to search for records after it
 * \return QVariantMap with systolic, diastolic, pulse and datetime fields of the found record.
 * Returns empty QVariantMap if the record is not found.
 */
QVariantMap DataAccessObject::retrieveFirstBloodPressureAfterDate(const QDate& date) const {
    return retrieveFirstBloodPressureBeyondDate(date, true);
}
