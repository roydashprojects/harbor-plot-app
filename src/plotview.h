/*
 * Medical diaries for Sailfish OS.
 * Copyright 2016-2017 EwerestMD LLC.
 * All rights reserved.
 */

#ifndef PLOTVIEW_H
#define PLOTVIEW_H

#include <QQuickPaintedItem>
#include <QObject>
#include <QDate>

#include "dataaccessobject.h"

/*!
 * \brief Class used to show blood pressure measurements on the plot.
 */
class PlotView : public QQuickPaintedItem {
    Q_OBJECT
    Q_PROPERTY(int periodIndex READ periodIndex WRITE setPeriodIndex NOTIFY periodIndexChanged)
    Q_PROPERTY(int offset READ offset WRITE setOffset NOTIFY offsetChanged)
    Q_PROPERTY(QString periodYears READ periodYears NOTIFY periodYearsChanged)
    Q_PROPERTY(bool plotVisible READ plotVisible NOTIFY plotVisibleChanged)
private:
    /*!
     * \brief Struct to contain the information used to build the line on the plot.
     */
    struct PlotLine {
        /*!
         * \brief List of points to draw on the plot.
         */
        QList<QVariantMap> plotPoints;
        /*!
         * \brief true if the record exists before the current period.
         */
        bool leftBoundRecordExists;
        /*!
         * \brief true if the record exists after the current period.
         */
        bool rightBoundRecordExists;
        /*!
         * \brief Color used to draw he line.
         */
        QColor color;
        /*!
         * \brief minimum value of all plot points.
         */
        float min;
        /*!
         * \brief maximum value of all plot points.
         */
        float max;
    };

    /*!
     * \brief Struct to contain the parameters of the time periods.
     */
    struct PlotPeriod {
        /*!
         * \brief length of the period in days.
         */
        int daysLength;
        /*!
         * \brief amount of days for which average values should be calculated.
         */
        int groupDays;
    };

    /*!
     * \brief List of plot periods available to display.
     */
    QList<PlotPeriod> plotPeriodList;

    /*!
     * \brief Dao instance used to retrieve the records.
     */
    DataAccessObject* daoInstance;

    /*!
     * \brief Index of the selected period.
     */
    int periodIndexValue;

    /*!
     * \brief amount of times for which current period is shifted from the latest one.
     */
    int offsetValue;

    /*!
     * \brief List of plot lines to draw on the plot.
     */
    QList<PlotLine> plotLineList;

    /*!
     * \brief Interval size between lines of the vertical scale.
     */
    float step;

    /*!
     * \brief Min value that can be displayed on the plot.
     */
    float minValue;

    /*!
     * \brief Max value that can be displayed on the plot.
     */
    float maxValue;

    void addPlotLine(const QVariantMap& leftBoundRecord, const QVariantMap& rightBoundRecord,
                     const QString& fieldName, const QColor& color);
    void fillPlotPointsList(PlotLine& plotLine, const QVariantMap& leftBoundRecord,
                            const QVariantMap& rightBoundRecord, const QString& fieldName);
    QList<QVariantMap> retrievePlotPointsList(const QString& fieldName) const;
    int verticalStepsCount() const;
    int horizontalStepsCount() const;
    float calculateMinValue(float step, float min, float max) const;
    int calculateTimeLabelWidth() const;
    int calculateTimeLabelHeight() const;
    void drawLines(QPainter* painter);
    void drawVerticalScaleLines(QPainter* painter);
    void drawVerticalScaleLabels(QPainter* painter);
    void drawHorizontalScaleMarkers(QPainter* painter);
    void drawHorizontalScaleLabels(QPainter* painter);
public:
    explicit PlotView(QQuickItem* parent = NULL);
    ~PlotView();
    Q_INVOKABLE void drawPlot();
    void calculatePlotAttributes();
    QPoint calculatePlotPoint(const QVariantMap& plotPoint) const;
    int calculatePlotXCoordinate(const QDateTime& datetime) const;
    int calculatePlotYCoordinate(float value) const;
    QDate calculatePeriodStartDate() const;
    QDate calculatePeriodEndDate() const;
    void setPeriodIndex(int periodIndex);
    int periodIndex() const;
    void setOffset(int offset);
    int offset() const;
    float getMinValue() const;
    float getMaxValue() const;
    float getStep() const;
    QString periodYears() const;
    Q_INVOKABLE void incrementOffset();
    Q_INVOKABLE void decrementOffset();
    Q_INVOKABLE bool plotVisible() const;
    void paint(QPainter* painter);
signals:

    /*!
     * \brief Signal sent on periodIndex property change.
     */
    void periodIndexChanged();

    /*!
     * \brief Signal sent on offset property change.
     */
    void offsetChanged();

    /*!
     * \brief Signal sent when drawPlot() method is called.
     */
    void plotVisibleChanged();

    /*!
     * \brief Signal sent when periodIndex or offset are changed.
     */
    void periodYearsChanged();
};

#endif // PLOTVIEW_H
