#ifndef DATAACCESSOBJECT_H
#define DATAACCESSOBJECT_H

#include <QObject>
#include <QFile>
#include <QTextStream>
#include <QDate>

class DataAccessObject : public QObject {
    Q_OBJECT
public:
    explicit DataAccessObject(QObject* parent = NULL);
    QList<QVariantMap> retrievePlotPoints(const QString& fieldName, const QDate&  startDate,
                                          const QDate& endDate, int groupDays) const;
    QList <QVariantMap> retrievePlotPointsForWeek(const QString& fieldName,
            const QDate& endDate) const;
    QVariantMap retrieveLastBloodPressureBeforeDate(const QDate& date) const;
    QVariantMap retrieveFirstBloodPressureAfterDate(const QDate& date) const;
private:
    void fillData();
    QList<QVariantMap> bloodPressureList;
    QVariantMap retrieveFirstBloodPressureBeyondDate(const QDate& date, bool lookAfter) const;
};

#endif // DATAACCESSOBJECT_H
