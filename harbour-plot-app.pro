# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-plot-app

CONFIG += sailfishapp qt c++11

SOURCES += src/harbour-plot-app.cpp \
    src/plotview.cpp \
    src/dataaccessobject.cpp

OTHER_FILES += qml/harbour-plot-app.qml \
    qml/cover/CoverPage.qml \
    qml/pages/FirstPage.qml \
    rpm/harbour-plot-app.changes.in \
    rpm/harbour-plot-app.spec \
    rpm/harbour-plot-app.yaml \
    translations/*.ts \
    harbour-plot-app.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 256x256

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/harbour-plot-app-de.ts

HEADERS += \
    src/plotview.h \
    src/dataaccessobject.h

QT += qml\
    quick

dataFiles.files = assets/blood_pressure_and_pulse_values.txt
dataFiles.path = /usr/share/$${TARGET}/assets
INSTALLS += dataFiles
